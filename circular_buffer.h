#ifndef INCLUDED_CIRCULAR_BUFFER
#define INCLUDED_CIRCULAR_BUFFER

#include <algorithm>
#include <cassert>
#include <vector>

template <typename T>
class CircularBuffer {
  public:
    class Iterator {
      private:
        CircularBuffer<T>* d_circularBuffer;
        size_t d_index;

      public:
        Iterator& operator++();
        Iterator& operator++(int);
        Iterator& operator--();
        Iterator& operator--(int);
        T &operator*() const;
        T *operator->() const;
    };

    // TODO: Add the other 3 iterators

  private:
    std::vector<T> d_buffer;
    size_t d_head;
    size_t d_tail;
    size_t d_size;

    size_t mod(size_t index) const;

  public:
    CircularBuffer();

    size_t size() const;
    size_t capacity() const;
    bool empty() const;
    T& front();
    const T& front() const;
    T& back();
    const T& back() const;
    T& operator[](size_t index);
    const T& operator[](size_t index) const;
    T& at(size_t index) const;

    Iterator begin() const;
    Iterator end() const;
    // TODO: Add the other 3 iterators

    void push_front(const T&);
    void push_back(const T&);
    void pop_front();
    void pop_back();
    void resize(size_t size);
    void clear();
};

template <typename T>
size_t CircularBuffer<T>::mod(size_t index) const
{
    const size_t cap = capacity();
    return (index + cap) % cap;
}

template <typename T>
CircularBuffer<T>::CircularBuffer() : d_buffer(), d_head(0), d_tail(0), d_size(0)
{
    d_buffer.resize(10);
}

template <typename T>
size_t CircularBuffer<T>::size() const
{
    return d_size;
}

template <typename T>
size_t CircularBuffer<T>::capacity() const
{
    return d_buffer.capacity();
}

template <typename T>
bool CircularBuffer<T>::empty() const
{
    return d_size == 0;
}

template <typename T>
T& CircularBuffer<T>::front()
{
    assert(!empty());

    return d_buffer[d_head];
}

template <typename T>
const T& CircularBuffer<T>::front() const
{
    assert(!empty());

    return d_buffer[d_head];
}

template <typename T>
T& CircularBuffer<T>::back()
{
    assert(!empty());

    return d_buffer[d_tail];
}

template <typename T>
const T& CircularBuffer<T>::back() const
{
    assert(!empty());

    return d_buffer[d_tail];
}

template <typename T>
void CircularBuffer<T>::push_front(const T& item)
{
    if (d_size == capacity()) {
        resize(d_size * 2);
    }

    if (!empty()) {
        d_head = mod(d_head - 1);
    }
    d_buffer[d_head] = item;
    ++d_size;
}

template <typename T>
void CircularBuffer<T>::push_back(const T& item)
{
    if (d_size == capacity()) {
        resize(d_size * 2);
    }

    if (!empty()) {
        d_tail = mod(d_tail + 1);
    }
    d_buffer[d_tail] = item;
    ++d_size;
}

template <typename T>
void CircularBuffer<T>::pop_front()
{
    assert(!empty());

    d_head = mod(d_head + 1);
    --d_size;
}

template <typename T>
void CircularBuffer<T>::pop_back()
{
    assert(!empty());

    d_tail = mod(d_tail - 1);
    --d_size;
}

template <typename T>
typename CircularBuffer<T>::Iterator
CircularBuffer<T>::begin() const
{
    return CircularBuffer<T>::Iterator(d_head);
}

template <typename T>
typename CircularBuffer<T>::Iterator
CircularBuffer<T>::end() const
{
    return CircularBuffer<T>::Iterator(d_tail);
}

template <typename T>
T& CircularBuffer<T>::operator[](size_t index)
{
    assert(!empty());

    return d_buffer[mod(d_head + index)];
}

template <typename T>
const T& CircularBuffer<T>::operator[](size_t index) const
{
    assert(!empty());

    return d_buffer[mod(d_head + index)];
}

template <typename T>
T& CircularBuffer<T>::at(size_t index) const
{
    assert(!empty());

    return d_buffer.at(mod(d_head + index));
}

template <typename T>
void CircularBuffer<T>::clear()
{
    d_head = 0;
    d_tail = 0;
    d_size = 0;
}

template <typename T>
void CircularBuffer<T>::resize(size_t size)
{
    std::rotate(d_buffer.begin(), d_buffer.begin() + d_head, d_buffer.end());
    d_buffer.resize(size);
    d_head = 0;
    d_tail = d_size - 1;
}

template <typename T>
bool operator==(const typename CircularBuffer<T>::Iterator& lhs,
                const typename CircularBuffer<T>::Iterator& rhs)
{
    return lhs.cur == rhs.cur;
}

template <typename T>
bool operator!=(const typename CircularBuffer<T>::Iterator& lhs,
                const typename CircularBuffer<T>::Iterator& rhs)
{
    return lhs.cur != rhs.cur;
}

#endif
