#include <circular_buffer.h>

#include <gtest/gtest.h>

TEST(circular_buffer, default_constructor_empty)
{
    CircularBuffer<int> empty;
    EXPECT_TRUE(empty.empty());
    EXPECT_EQ(0, empty.size());
}

TEST(circular_buffer, push_back)
{
    CircularBuffer<int> cb;
    cb.push_back(1);
    EXPECT_FALSE(cb.empty());
    EXPECT_EQ(1, cb.size());
    cb.push_back(2);
    EXPECT_EQ(2, cb.size());
}

TEST(circular_buffer, back)
{
    CircularBuffer<int> cb;
    cb.push_back(1);
    EXPECT_EQ(1, cb.back());
    cb.push_back(2);
    EXPECT_EQ(2, cb.back());
}

TEST(circular_buffer, push_front)
{
    CircularBuffer<int> cb;
    cb.push_front(1);
    EXPECT_FALSE(cb.empty());
    EXPECT_EQ(1, cb.size());
    cb.push_front(2);
    EXPECT_EQ(2, cb.size());
}

TEST(circular_buffer, front)
{
    CircularBuffer<int> cb;
    cb.push_front(1);
    EXPECT_EQ(1, cb.front());
    cb.push_front(2);
    EXPECT_EQ(2, cb.front());
}

TEST(circular_buffer, front_back)
{
    CircularBuffer<int> cb;
    cb.push_front(1);
    EXPECT_EQ(1, cb.front());
    EXPECT_EQ(1, cb.back());
    cb.push_front(2);
    EXPECT_EQ(2, cb.front());
    EXPECT_EQ(1, cb.back());
}

TEST(circular_buffer, push_front_many_resize)
{
    CircularBuffer<int> cb;
    cb.push_front(1);
    EXPECT_EQ(1, cb.front());
    cb.push_front(2);
    EXPECT_EQ(2, cb.front());
    cb.push_front(3);
    EXPECT_EQ(3, cb.front());
    cb.push_front(4);
    EXPECT_EQ(4, cb.front());
    cb.push_front(5);
    EXPECT_EQ(5, cb.front());
    cb.push_front(6);
    EXPECT_EQ(6, cb.front());
    cb.push_front(7);
    EXPECT_EQ(7, cb.front());
    cb.push_front(8);
    EXPECT_EQ(8, cb.front());
    cb.push_front(9);
    EXPECT_EQ(9, cb.front());
    cb.push_front(10);
    EXPECT_EQ(10, cb.front());
    cb.push_front(11);
    EXPECT_EQ(11, cb.front());
    EXPECT_EQ(1, cb.back());
}

TEST(circular_buffer, subscript)
{
    CircularBuffer<int> cb;
    cb.push_back(1);
    EXPECT_EQ(1, cb[0]);
    cb.push_back(2);
    EXPECT_EQ(2, cb[1]);
}
